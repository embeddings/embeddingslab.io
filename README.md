# Visualizing embedding networks

This is the code for our tiny app meant to visualize embedding networks and their data-inherited bias from Google News. 

Example searches:

- https://embeddings.gitlab.io/#she,woman,housewife,shopkeeper,cashier
- https://embeddings.gitlab.io/#coloured,negro,zulu
- https://embeddings.gitlab.io/#wealth,resources,manpower





