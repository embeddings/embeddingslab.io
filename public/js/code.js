
var EmbeddingsNetwork = (function() {
  'use strict';

  var NR_NEIGHBOURS = 10;
  var CURRENT_GRAPH_URL = window.location.href;
  var ADDED_WORDS = new Set();
  var NORMS = {};

  var options = {
            autoResize: true,
            layout: {
                improvedLayout:false
            },
            nodes: {
                shape: 'dot',
                color: '#46bb57',
                size: 16
            },
            physics: {
                forceAtlas2Based: {
                    gravitationalConstant: -38,
                    centralGravity: 0.005,
                    springConstant: 0.18
                },
                maxVelocity: 146,
                solver: 'forceAtlas2Based',
                timestep: 0.35,
                stabilization: {
                    iterations: 150
                }
            }
        };
    
  var nodes, edges, network;

  if(typeof(String.prototype.trim) === "undefined")
  {
      String.prototype.trim = function() 
      {
          return String(this).replace(/^\s+|\s+$/g, '');
      };
  }

  function norm(a) {
    return Math.sqrt(a.reduce(function(sum, val) {
      return sum + val*val;  
    }, 0));
  }

  function preComputeNorm() {
    for (var word in EMBEDDINGS) {
      var nrm = norm(EMBEDDINGS[word]);
      NORMS[word] = nrm;
    }
  }

  function cosineSimilarity(vec1, vec2, N1, N2) {
    return Math.abs(vec1.reduce(function(sum, a, idx) {
      return sum + a * vec2[idx]; }, 0) / (N1 * N2)); 
  }

  function getSimilarities(word) {
    var vector = EMBEDDINGS[word];
    var N1 = NORMS[word];
    var similarities = [];
    for (var eachWord in EMBEDDINGS) {
      var similarity = cosineSimilarity(vector, EMBEDDINGS[eachWord], N1, NORMS[eachWord]);
      similarities.push([eachWord, similarity]);
    }
    similarities.sort(function(a, b) {
      return b[1] - a[1]; 
    });
    return similarities.slice(0, NR_NEIGHBOURS);
  }

  function updateURL(word) {
    var separator = '#'
    if (CURRENT_GRAPH_URL.indexOf("#") !== -1)
      separator = ',';
    if (ADDED_WORDS.has(word) === false)
    {
        ADDED_WORDS.add(word);
        CURRENT_GRAPH_URL += separator + word;
        var url = document.getElementById('url');
        url.href = CURRENT_GRAPH_URL;
    }   
  }

  function baseURL() {
    var idx = CURRENT_GRAPH_URL.indexOf("#");
    if (idx === -1)
      return CURRENT_GRAPH_URL;
    else
      return CURRENT_GRAPH_URL.substr(0, idx);   
  }

  function initEmptyNet() {
      preComputeNorm();
      CURRENT_GRAPH_URL = baseURL();
      var container = document.getElementById('mynetwork');
      nodes = new vis.DataSet([]);
      edges = new vis.DataSet([]);
      var data = {
        nodes: nodes,
        edges: edges
      };
      network = new vis.Network(container, data, options);
    }

  function displaySearchWords(words) {
    var i;
    for (i = 0; i < words.length; i++)
    {
      if (!(words[i].trim() in EMBEDDINGS))
        continue;
      var simWords = getSimilarities(words[i].trim());
      updateURL(words[i].trim());
      renderSimilarities(simWords);
    }
  }

  function initializeAll() {
    var parameters = window.location.hash.substr(1);
    if (parameters) {
      var words = parameters.toLowerCase().split(',');
      if (words.length > 0) {
        initEmptyNet();
        displaySearchWords(words);
      }
    }
    document.getElementById('word-search').addEventListener('keyup', function(event) 
    {
      if (event.keyCode == 13) 
      {
        initEmptyNet();
        var word = document.getElementById('word-search').value;
        word = word.trim().toLowerCase();
        if (!(word in EMBEDDINGS)) {
          document.getElementById('message').innerHTML = 'Nothing for that word. Try something else.';
          return;
        }

        var simWords = getSimilarities(word);
        updateURL(word);
        document.getElementById('message').innerHTML = '';
        renderSimilarities(simWords);
      }
    });
  }

  function addNode(word) {
    if (nodes.get(word) == null)
      nodes.add({'id': word, 'label': word})
  }

  function addEdge(from, to, score) {
    if (network.getConnectedNodes(from).indexOf(to) < 0)
      edges.add({'from': from, 'length':score, 'to': to})
  }

  function addSimilarities(sims, curr_idx) {
    var i; 
    addNode(sims[curr_idx][0])
    for (i = 1; i < sims.length; i++) { 
      addNode(sims[i][0])
      addEdge(sims[curr_idx][0], sims[i][0], -Math.log(sims[i][1])*10)
    }
  }

  function renderSimilarities(sims) {
    var curr_idx = 0;
    addSimilarities(sims, curr_idx);

    network.on("selectNode", function (params) {
        //console.log('Node id:', params['nodes'][0]);
        var curr_idx = params['nodes'][0];
        var selected_node = nodes.get(curr_idx);
        updateURL(selected_node['label']);
        var sims_i = getSimilarities(selected_node['label']);
        addSimilarities(sims_i, 0);
    });
    //network.on("oncontext", function (params) {
    //    alert(CURRENT_GRAPH_URL);
    //});
  }

  return {
    init: initializeAll
  };
})();
    
window.addEventListener('load', EmbeddingsNetwork.init);
